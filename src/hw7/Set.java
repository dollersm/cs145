package hw7;

import java.util.ArrayList;

public class Set {
	
	private ArrayList<String> set;
	
	public Set() {
		set =  new ArrayList<String>();
	}
	
	public int size(){
		return set.size();
	}
	
	public String get(int i) {
		return set.get(i);
	}
	
	public boolean has(String mayContain) {
		for(int i = 0; i < set.size(); i++) {
			if (set.get(i).equals(mayContain)){
				return true;
			}
		}
		return false;
	}
	
	public void add(String toAdd) {
		if(!has(toAdd)) {
			set.add(toAdd);
		}
	}
	
	public String toString() {
		String result = "";
		for(int i = 0; i < set.size()-1; i++) {
			result += set.get(i) + System.lineSeparator();
		}
		if(set.size() > 0) {
			result += set.get(set.size()-1);
		}
		return result;
	}
	
}
