package hw7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Scanner;

public class DondeUtilities {
	
	public static void main(String[] args) 
			throws MalformedURLException, NoSuchPlaceException, IOException, URISyntaxException {
		System.out.println(readCSV(new File("/Users/owner/Documents/cs145/places.txt")));
	}
	
	public static PlacesCache readCSV(File file) 
			throws MalformedURLException, IOException, URISyntaxException, FileNotFoundException{
		PlacesCache pc = new PlacesCache();
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(file);
		while(scan.hasNextLine()) {
			String tempPlace = scan.nextLine();
			String[] tempArray = tempPlace.split("\\|", -1);
			try {
				pc.getPlace(tempArray[1]);
				pc.getPlace(tempArray[1]).addPerson(tempArray[0]);
				System.out.println(pc.getPlace(tempArray[1]) + " " +tempArray[1]);
			} catch (Exception e) {
				
			};
		}
		return pc;
	}
	

	public static void writeKML(PlacesCache place, File file) 
			throws FileNotFoundException {
		PrintWriter write = new PrintWriter(file);
		write.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		write.println("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
		write.println("<Document>");
		write.println("<name>Donde</name>");
		for(int i = 0; i < place.size(); i++) {
			/*try {
				place.get(i);
				write.println(new Place(place.get(i).getName()).toKML());
			} catch (Exception e) {
				
			}*/
			write.println(place.get(i).toKML());
		}
		write.println("</Document>");
		write.println("</kml>");
		write.close();
	}
}
