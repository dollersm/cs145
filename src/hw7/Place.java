package hw7;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Place {
	private String name;
	private double latitude;
	private double longitude;
	private Set peopleInPlace;
	private String peopleList;
	
	public Place(String name, double latitude, double longitude) {
		this.peopleInPlace = new Set();
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.peopleList = "";
	}
	
	public Place(String name) 
			throws NoSuchPlaceException, MalformedURLException, IOException, URISyntaxException{
		this.peopleInPlace = new Set();
		this.name = name;
		this.peopleList = "";
		try {
			String place = WebUtilities.slurpURL(toGeocodeURL());
			Scanner scan = new Scanner(place);
			
			this.latitude = scan.nextDouble();
			this.longitude = scan.nextDouble();
			
		} catch (Exception e) {
			throw new NoSuchPlaceException(name);
		}
	}
	
	public URL toGeocodeURL() 
			throws MalformedURLException, URISyntaxException, NoSuchPlaceException{
		URI uri = new URI("http", "www.twodee.org", "/teaching/cs145/2016c/homework/hw7/geocode.php", "place=" + name, null);
		return uri.toURL();
	}
	
	public void addPerson(String personName) {
			peopleInPlace.add(personName);
		
	}
	
	public String toKML() {
		this.peopleList = peopleInPlace.toString();
		return String.format("<Placemark>%n<name>%s</name>%n<description>%s</description>%n<Point><coordinates>%.2f,%.2f</coordinates></Point>%n</Placemark>", name, peopleList, longitude, latitude);
	}
	
	public String getName() {
		return this.name;
	}
	
	public double getLatitude() {
		return this.latitude;
	}
	
	public double getLongitude() {
		return this.longitude;
	}
}
