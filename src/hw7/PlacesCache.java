package hw7;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class PlacesCache {
	
	private ArrayList<Place> cache;
	private Set set;
	
	public PlacesCache() {
		this.set = new Set();
		this.cache = new ArrayList<Place>();
	}
	
	public boolean isCached(String name) {
		for(int i = 0 ; i < cache.size(); i ++) {
			if(cache.get(i).getName().equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	public Place getPlace(String name) 
			throws NoSuchPlaceException, MalformedURLException, IOException, URISyntaxException{
		if (!isCached(name)) {
			Place newPlace = new Place(name);
			cache.add(newPlace);
			return newPlace;
		} else{
			for(int i = 0 ; i < cache.size(); i ++) {
				if(cache.get(i).getName().equals(name)) {
					return cache.get(i);
				} 
			}
		}
		return null;
	}
	
	public int size() {
		return cache.size();
	}
	
	public Place get(int i) {
		try {
			cache.get(i);
		} catch(Exception e) {
			System.err.println("IndexOutOfBoundException");
		}
		return cache.get(i);
	}
	
}
