package hw7;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;
import java.io.InputStream;

public class WebUtilities {
	
	public static String slurpURL(URL url) throws IOException {
		URLConnection conn = url.openConnection();
		InputStream is = conn.getInputStream();
		Scanner scan = new Scanner(is);
		String urlRead = scan.nextLine();
		while(scan.hasNextLine()) {
			urlRead = urlRead + scan.nextLine();
		}
		return urlRead;
	}
}
