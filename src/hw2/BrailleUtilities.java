package hw2;

public class BrailleUtilities {
	
	public static final String RAISED = "\u2022";
	public static final String UNRAISED = "\u00b7";
	public static final String LETTER_SPACER = "  ";
	public static final String WORD_SPACER = "    ";

	public static void main(String[] args) {
		
	}
	public static String translateTopLine(String textToTranslate) { 
		String replace =  textToTranslate.replaceAll(" ", WORD_SPACER).toLowerCase().replaceAll("[abehkloruvz]", (RAISED + UNRAISED + LETTER_SPACER)).replaceAll("[cdfgmnpqxy]", (RAISED + RAISED + LETTER_SPACER)).replaceAll("[ijstw]", (UNRAISED + RAISED + LETTER_SPACER)).trim();
		return replace;
	}
	public static String translateMiddleLine(String textToTranslate) { 
		String replace = textToTranslate.replaceAll(" ", WORD_SPACER).toLowerCase().replaceAll( "[ackmux]", (UNRAISED + UNRAISED + LETTER_SPACER)).replaceAll("[bfilpsv]", (RAISED + UNRAISED + LETTER_SPACER)).replaceAll("[ghjqrtw]", (RAISED + RAISED + LETTER_SPACER)).replaceAll("[denoyz]", (UNRAISED + RAISED + LETTER_SPACER)).trim();
		return replace;
	}
	public static String translateBottomLine(String textToTranslate) { 
		String replace = textToTranslate.replaceAll(" ", WORD_SPACER).toLowerCase().replaceAll( "[abcdefghij]", (UNRAISED + UNRAISED + LETTER_SPACER)).replaceAll("[klmnopqrst]", (RAISED + UNRAISED + LETTER_SPACER)).replaceAll("[uvxyz]", (RAISED + RAISED + LETTER_SPACER)).replaceAll("[w]", (UNRAISED + RAISED + LETTER_SPACER)).trim();
		return replace.trim();
	}
	public static String translate (String textToTranslate) { 
		String i = translateTopLine(textToTranslate);
		String j = translateMiddleLine(textToTranslate);
		String k = translateBottomLine(textToTranslate);
		return String.format("%s%n%s%n%s%n", i, j ,k);
	}
}
