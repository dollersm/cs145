package hw2;

public class NumberUtilities {
	public static void main(String[] args) {
		
	}
	
	public static int round10 (double number2Round) {
		return (int)(Math.round(number2Round / 10.0) * 10.0);
	}
	public static int getGameCount(int nOfRounds) {
		return (int) (-1 + Math.pow(2, nOfRounds));
	}
	public static double getFraction(double number2Slice) {
		return Math.abs(number2Slice - (int) number2Slice);
	}
}
