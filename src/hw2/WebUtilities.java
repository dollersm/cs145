package hw2;

public class WebUtilities {
	public static void main(String[] args) {
		
	}

	public static String getImageLink(String path, int width, int height) {
		return ("<a href=\"" + path + "\"><img src=\"" + path + "\" width=\"" + width + "\" height=\"" + height + "\"></a>");
	}
	public static String getHost (String url) { 
		int index = url.indexOf("://");
		url = url.substring(index + 3);
		index = url.indexOf('/');
		return url.substring(0, index);
	}
	public static String getTitle(String html) {
		return html.substring(html.indexOf("<title>") + 7, html.indexOf("</title"));
	}
	public static String invertHexColor (String hex){
		int r = Integer.valueOf(hex.substring( 1, 3 ), 16 );
        int g = Integer.valueOf(hex.substring( 3, 5 ), 16 );
        int b = Integer.valueOf(hex.substring( 5, 7 ), 16 );
        int oppR = 255 - r;
        int oppG = 255 - g;
        int oppB = 255 - b;
        String fixedR = String.format("%02x", oppR);
        String fixedG = String.format("%02x", oppG);
        String fixedB = String.format("%02x", oppB);
		return "#" + fixedR + fixedG + fixedB;
	}
	
}

