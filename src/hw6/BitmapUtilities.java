package hw6;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;
import javax.imageio.ImageIO;

public class BitmapUtilities {
	
	public static boolean[][] create(int width, int height){
		boolean[][] bitmap = new boolean[height][width];
		return bitmap;
	}
	
	public static void randomize(boolean[][] bitmap, long seed){
		Random random = new Random(seed);
		for(int i = 0; i < bitmap.length; i++) {
			for(int j = 0; j < bitmap[0].length;j++) {
				bitmap[i][j] = random.nextBoolean();
			}
		}
	}
	
	public static void write(boolean[][] bitmap, File file) 
			throws FileNotFoundException {
		PrintWriter out = new PrintWriter(file);
		out.println("P1");
		out.println(bitmap[0].length + " " + bitmap.length);
		for(int i = 0; i < bitmap.length; i++) {
			for(int j = 0; j < bitmap[0].length;j++) {
				if(bitmap[i][j]) {
					out.println(0);
				} else {
					out.println(1);
				}
			}
		}
		out.close();
	}
	
	public static BufferedImage toBufferedImage(boolean[][] bitmap) {
		BufferedImage image = new BufferedImage(bitmap[0].length, bitmap.length, BufferedImage.TYPE_BYTE_BINARY);
		for(int i = 0; i < bitmap.length; i++) {
			for(int j = 0; j < bitmap[0].length;j++) {
				if(bitmap[i][j]) {
					image.setRGB(j, i, Color.BLACK.getRGB());
				} else {
					image.setRGB(j, i, Color.WHITE.getRGB());
				}
			}
		}
		return image;
	}
	
	public static boolean equals(boolean[][] bitmap1, boolean[][] bitmap2) {
		if(bitmap1.length != bitmap2.length && bitmap1[0].length != bitmap2[0].length) {
			return false;
		}
		for(int i = 0; i < bitmap1.length; i++) {
			for(int j = 0; j < bitmap1[0].length;j++) {
				if(bitmap1[i][j] != bitmap2[i][j]) {
					return false;
				}
			}
		}
		return true;
	}
	
	public static boolean[][] clone(boolean[][] bitmap) {
		boolean[][] clone = new boolean[bitmap.length][bitmap[0].length];
		for(int i = 0; i < bitmap.length; i++) {
			for(int j = 0; j < bitmap[0].length;j++) {
				clone[i][j] = bitmap[i][j];
			}
		}
		return clone;
	}
	
	public static int wrapIndex(int upperBound, int index) {
		if(index >= 0 && index < upperBound) {
			return index;
		}else if(index >= upperBound) {
			return (index % upperBound);
		} else if(index % upperBound == 0) {
			return 0;
		} else {
			return ((index % upperBound) + upperBound);
		}
	}
	
	public static boolean isOn(boolean[][] bitmap, int colDex, int roDex) {
		colDex = wrapIndex(bitmap[0].length, colDex);
		roDex = wrapIndex(bitmap.length, roDex);
		return bitmap[roDex][colDex];
	}
}
