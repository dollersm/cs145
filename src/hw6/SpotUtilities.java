package hw6;

import java.io.File;
import java.io.IOException;

import hw6.speccheck.GifSequenceWriter;

public class SpotUtilities {

	public static int[] countNeighbors(boolean[][] bitmap, int horizontalRadiusOut, int verticalRadiusOut, int horizontalRadiusInner, int verticalRadiusInner, int colDex, int roDex) {
		int[] neighbor = new int[2];
		for (int r = (roDex - verticalRadiusOut); r <= (roDex + verticalRadiusOut); r++) {
			for (int c = (colDex - horizontalRadiusOut); c <= (colDex + horizontalRadiusOut); c++) {
				double deltCol = c - colDex;
				double deltRow = r - roDex;
				double outDist = (Math.pow(deltCol, 2.0) / Math.pow(horizontalRadiusOut, 2.0)) + (Math.pow(deltRow, 2.0) / Math.pow(verticalRadiusOut, 2.0));
				double innerDist = (Math.pow(deltCol, 2.0) / Math.pow(horizontalRadiusInner, 2.0)) + (Math.pow(deltRow, 2.0) / Math.pow(verticalRadiusInner, 2.0));
				if(BitmapUtilities.isOn(bitmap, c, r)){
					if(outDist <=1.0) {
						if(innerDist <= 1.0) {
							neighbor[0]++;
						} else {
							neighbor[1]++;
						}
					}	
				}
			}
		}
		return neighbor;
	}
	
	public static boolean[][] step(boolean[][] bitmap, int horizontalRadiusOut, int verticalRadiusOut, int horizontalRadiusInner, int verticalRadiusInner, double proportion) {
		int height = bitmap.length;
		int width = bitmap[0].length;
		boolean [][] newBitmap = new boolean[height][width];
		for (int i = 0; i < height; i++) {
			for(int j = 0; j < width; j++) {
				int[] count = countNeighbors(bitmap, horizontalRadiusOut, verticalRadiusOut, horizontalRadiusInner, verticalRadiusInner, j, i);
				double difference = count[0] - (proportion * count[1]);
				if(Math.abs(difference) < 0.001) {
					newBitmap[i][j] = bitmap[i][j];
				} else if(difference > 0) {
					newBitmap[i][j] = true;
				} else {
					newBitmap[i][j] = false;
				}
			}
		}
		return newBitmap;
	}
	
	public static int converge(boolean[][] bitmap, int horizontalRadiusOut, int verticalRadiusOut, int horizontalRadiusInner, int verticalRadiusInner, double proportion, File GIF, int maxSteps) {
		GifSequenceWriter out = new GifSequenceWriter(GIF, 200, true);
		out.appendFrame(BitmapUtilities.toBufferedImage(bitmap));
		boolean[][] newBitmap = SpotUtilities.step(bitmap, horizontalRadiusOut, verticalRadiusOut, horizontalRadiusInner, verticalRadiusInner, proportion);
		int nSteps = 0;
		for(nSteps = 0; nSteps < maxSteps; nSteps++) {
			if(BitmapUtilities.equals(bitmap, newBitmap)) {
				out.appendFrame(BitmapUtilities.toBufferedImage(newBitmap));
				out.close();
				return nSteps + 2;
			}
			out.appendFrame(BitmapUtilities.toBufferedImage(newBitmap));
			bitmap = BitmapUtilities.clone(newBitmap);
			newBitmap = SpotUtilities.step(bitmap, horizontalRadiusOut, verticalRadiusOut, horizontalRadiusInner, verticalRadiusInner, proportion);
		}
		out.appendFrame(BitmapUtilities.toBufferedImage(newBitmap));
		out.close();
		return nSteps + 1;
		}
}
