package hw5;

import java.util.Arrays;

import hw5.speccheck.Accidental;
import hw5.speccheck.Duration;
import hw5.speccheck.Letter;
import hw5.speccheck.Note;

public class MusicUtilities {

	public static void main(String[] args) {
		int[] test = {1,3,5};
		System.out.println(Arrays.toString(getScale(new Note("4C5"), test)));
	}
	
	public static Note[] getScale (Note note, int[] halfstepOffsets) {
		Note[] notesToScale = new Note[halfstepOffsets.length + 1];
		notesToScale[0] = note;
		for (int i = 1; i < halfstepOffsets.length; i++) {
			notesToScale[i]= new Note(note.getHalfstepID() + halfstepOffsets[i - 1], note.getDuration(), note.getAmplitude(), note.isDotted());
		}
		return notesToScale;
	}
	
	public static Note[] getMajorScale (Note note) {
		Note[] scale = new Note[8];
		int halfsteps = 0;
		for (int i = 0; i < 8; i ++) {
			scale[i] = new Note(note.getHalfstepID() + halfsteps, note.getDuration());
			if (scale[i].getLetter() == Letter.E || scale[i].getLetter() == Letter.B) {
				halfsteps = halfsteps + 1;
			} else {
				halfsteps = halfsteps + 2;
			}
		}
		return scale;
	}
	
	public static Note[] getMinorPentatonicBluesScale (Note note) {
		Note[] scale = new Note[6];
		int offset = 0;
		for (int i = 0; i < 6; i ++) {
			scale[i] = new Note(note.getHalfstepID() + offset, note.getDuration());
			if (scale[i].getLetter() == Letter.C || scale[i].getLetter() == Letter.G) {
				offset = offset + 3;
			} else {
				offset = offset + 2;
			}
		}
		return scale;
	}
	
	public static Note[] getBluesScale (Note note) {
		Note[] scale = new Note[7];
		int offsets = 0;
		for (int i = 0; i < 7; i ++) {
			scale[i] = new Note(note.getHalfstepID() + offsets, note.getDuration());
			if (scale[i].getLetter() == Letter.C || (scale[i].getLetter() == Letter.G && scale[i].getAccidental() == Accidental.NATURAL)) {
				offsets = offsets + 3;
			} else if ((scale[i].getLetter() == Letter.E && scale[i].getAccidental() == Accidental.FLAT) || (scale[i].getLetter() == Letter.B && scale[i].getAccidental() == Accidental.FLAT)) {
				offsets = offsets + 2;
			} else { 
				offsets = offsets + 1;
			}
			
		}
		return scale;
	}
	
	public static Note[] getNaturalMinorScale(Note note) {
		Note[] scale = new Note[8];
		int offset = 0;
		for (int i = 0; i < 8; i ++) {
			scale[i] = new Note(note.getHalfstepID() + offset, note.getDuration());
			if (scale[i].getLetter() == Letter.D || (scale[i].getLetter() == Letter.G && scale[i].getAccidental() == Accidental.NATURAL)) {
				offset = offset + 1;
			} else {
				offset = offset + 2;
			}
		}
		return scale;
	}
	
	public static Note[] join(Note[] first, Note[] second) {
		Note[] joined = new Note[first.length + second.length - 1];
		for(int i = 0; i < joined.length; i++) {
			for(int j = 0; j < first.length - 1; j++){
				joined[i] = first[i];
			}
			for(int j = 0; j < second.length; j++){
				joined[i] = second[j];
			}
		}
		return joined;
	}
	
	public static Note[] repeat(Note[] noteArray, int repeatN) {
		Note[] repeated = new Note[noteArray.length * repeatN];
		for(int i = 0; i < repeated.length; i++) {
			for(int j = 0; j < noteArray.length;j++){
				repeated[i]= noteArray[j];
			}
		}
		return repeated;
	}
	
	public static Note[] ramplify(Note[] noteArray, double starting, double ending) {
		Note[] ramped = new Note[(int)(Math.abs((1+ (ending - starting) * 10)))];
		for (int i = 0; i < ramped.length; i++) {
			ramped[i] = new Note(noteArray[i].getLetter(), noteArray[i].getOctave(), noteArray[i].getDuration(), noteArray[i].getAmplitude() * starting + (0.1 * i));
		}
		return ramped;
	}
	
	public static Note[] reverse(Note[] notes) {
		for(int i = 0; i < notes.length / 2; i++){
		    Note temp = notes[i];
		    notes[i] = notes[notes.length - i - 1];
		    notes[notes.length - i - 1] = temp;
		}
		return notes;
	}
	
	public static Note[] transpose(Note[] noteArray, Note note){
		int difference = noteArray[0].getHalfstepID() - note.getHalfstepID();
		Note[] transposed = new Note[noteArray.length];
		for(int i = 0; i < transposed.length ; i++) {
			transposed[i] = new Note(noteArray[i].getLetter(), noteArray[i].getAccidental(), noteArray[i].getHalfstepID() + difference, noteArray[i].getDuration());
		}
		return transposed;
	}
	
	public static Note[] invert(Note[] noteArray, Note note) {
		int pivot = noteArray[0].getHalfstepID() - note.getHalfstepID();
		Note[] inverted = new Note[noteArray.length];
		for(int i = 0; i < inverted.length ; i++) {
			inverted[i] = new Note(noteArray[i].getLetter(), noteArray[i].getAccidental(), noteArray[i].getHalfstepID() - pivot, noteArray[i].getDuration());
		}
		return inverted;
	}

}
