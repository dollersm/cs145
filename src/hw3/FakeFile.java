package hw3;

import java.io.File;

	public class FakeFile extends File {
		  private long millis;

		  public FakeFile(String file) {
		    super(file);
		  }

		  @Override
		  public boolean setLastModified(long millis) {
		    this.millis = millis;
		    return true;
		  }

		  @Override
		  public long lastModified() {
		    return millis;
		  }
		}

