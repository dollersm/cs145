package hw3;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;



public class Trutilities {

	public static void main(String[] args) throws NumberFormatException {
		FakeFile ff = new FakeFile("foo.jpg"); 
		ff.setLastModified(1000);
		System.out.println(isOld(ff, 2010, 11, 9));
	}
	public static boolean isOrdered(int first, int second, int third, int fourth, int fifth) {
		return (first <= second && second <= third && third <= fourth && fourth <= fifth) || (first >= second && second >= third && third >= fourth && fourth >= fifth);
	}	
	
	public static boolean isGreenish (String hex) {
		int red = Integer.valueOf(hex.substring(1, 3), 16 );
		int green = Integer.valueOf(hex.substring(3, 5), 16 );
		int blue = Integer.valueOf(hex.substring(5, 7), 16 );
		return green > red && green > blue;
	}
	
	public static boolean isMilitary (String time) {
		int timeHours = Integer.parseInt((String) (time.subSequence(0, 2)));
		int timeMinutes = Integer.parseInt((String) (time.subSequence(2, 4)));
		if (timeHours < 24){
			if (timeMinutes < 60) {
				return true;
			}
		} else {
			return false;
		}
		return false;
	}
	
	public static boolean isImage (File file) {
		String name = file.getName().toLowerCase();
		return name.endsWith(".png") || name.endsWith(".gif") || name.endsWith(".jpg")|| name.endsWith(".jpeg");
	}
	
	public static boolean hasMultipleDots (String dots) {
		return dots.indexOf('.', dots.indexOf('.') + 1) != -1;
	}
	
	public static boolean fitsAspect (int width, int height, double aspectRatio) {
		double x = (1.0 * width / height);
		DecimalFormat df = new DecimalFormat(".##");
		return  df.format(x).equals(df.format(aspectRatio));
		
	}
	
	public static boolean fitsWithin(int wOfRectA, int hOfRectA, int wOfRectB, int hOfRectB) {
		int rectA = (2 * (wOfRectA)) * (2 * (hOfRectA));
		int rectB = (2 * (wOfRectB)) * (2 * (hOfRectB));
		return rectA <= rectB;
	}
	
	public static boolean isFaster(String timeA, String timeB) throws NumberFormatException {
		int timeAHours = Integer.parseInt(timeA.substring(0,timeA.indexOf(":")));
		timeAHours = timeAHours * 60;
		int timeBHours = Integer.parseInt(timeB.substring(0,timeB.indexOf(":")));
		timeBHours = timeBHours * 60;
		int timeAMinutes = Integer.parseInt(timeA.substring(timeA.indexOf(":") + 1,4));
		int timeBMinutes = Integer.parseInt(timeB.substring(timeB.indexOf(":") + 1,4));
		return (timeAHours + timeAMinutes) < (timeBHours + timeBMinutes);
	}
	
	public static boolean isIllegal (String cardinal) {
		cardinal = cardinal.toLowerCase();
		return !(cardinal.equals("north") || cardinal.equals("east") || cardinal.equals("south") || cardinal.equals("west"));
	}
	
	public static boolean isOld(File file, int year, int month, int day) {
		Calendar dateToCompare = Calendar.getInstance();
		dateToCompare.set(year, month - 1, day);
		long compare = dateToCompare.getTimeInMillis();
		long time = file.lastModified();
		return compare >= time;
	}
}
