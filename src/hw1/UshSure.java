package hw1;

import java.util.*;

public class UshSure {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
	
		System.out.print("Ticket number? ");
		int ticketNumber = input.nextInt();
		
		System.out.print("Seats in a row? ");
		int seatsInRow = input.nextInt();
		
		int amountOfRows = (int) (ticketNumber / seatsInRow);
		int seatInColumn = ticketNumber % seatsInRow;
		char rowLetter = (char) (amountOfRows + 65);
		
		System.out.println("Seat: " + rowLetter + seatInColumn);
	}

}
