package hw1;

import java.util.Scanner;

public class Shuriken {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Spin? ");
		double spin = scan.nextDouble();
		int t = 0;
		while (t <= 360) {
			double x = (1.1 + Math.sin(Math.toRadians(4 * t))) * Math.cos(Math.toRadians(t + spin));
			double y = (1.1 + Math.sin(Math.toRadians(4 * t))) * Math.sin(Math.toRadians(t + spin));
			System.out.printf("%.2f,%.2f\n", x, y);
			t = t + 30;
		}	
	}
	
}
