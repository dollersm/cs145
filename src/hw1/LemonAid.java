package hw1;

import java.util.Scanner;

public class LemonAid {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner console = new Scanner(System.in);
		
		System.out.print("How many parts lemon juice? ");
		int partsLemon = console.nextInt();
		
		System.out.print("How many parts sugar? ");
		int partsSugar = console.nextInt();
		
		System.out.print("How many parts water? ");
		int partsWater = console.nextInt();
		
		System.out.print("How many cups of lemonade? ");
		int cupsOfLemonade = console.nextInt();
		
		int partsTotal = partsLemon + partsSugar + partsWater;
		double ratioOfLemon =  (1.0 * cupsOfLemonade * partsLemon / partsTotal);
		double ratioOfSugar = (1.0 * cupsOfLemonade * partsSugar / partsTotal);
		double ratioOfWater = (1.0 *cupsOfLemonade * partsWater / partsTotal);
		
		System.out.println("Amounts (in cups):");
		System.out.println("  Lemon juice: " + (ratioOfLemon));
		System.out.println("  Sugar: " + (ratioOfSugar));
		System.out.println("  Water: " + (ratioOfWater));

	}
}
